<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SQL REPLACE</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/theme.css">
		<style>

			body,html {
				height: 100%;
				width: 100%;
				margin: 0;
				padding: 0;
				color: #303030;
				font-family: 'Roboto', sans-serif;
				font-size: 15px;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
			}

			.logo{
				text-align: center;
			}

			form {
				margin-top:  70px;
				width: 57%;
				margin-left: auto;
				margin-right: auto;
			}

			.form-group{
				display: block;
				margin-bottom: 10px;
				overflow: hidden;
			}

			label{
				width: 11%;
				display: block;
				float: left;
				text-align: left;
				font-size: 13px;	
				height: 39px;
				line-height: 42px;
			}

			input[type="text"] {
			  width: 35%;
			  float: left;
			  background: transparent;
			  border: none;
			  box-shadow: 0 2px 0 #476edb;
			  height: 35px;
			  line-height: 35px;
			  transition: all 0.4s;
			  color: #303030;
			  margin-bottom: 4px;
			  display: block;
			}
			input[type="text"]:focus {
			  outline: none;
			}

			.buttons{
				width: 52%;
				display: block;
				padding-top: 7px;
				text-align: left;
			  	float: right;
			}

			.buttons button{
				background: #476edb;
				border: solid 2px #476edb;
				padding: 8px;
				border: solid 2px #476edb;
				color: white;
				font-weight: bold;
				transition: all 0.6s;
				margin-right: 5px;
			}

			input[type="submit"] {
			  background: #476edb;
			  width: 200px;
			  margin-top: 20px;
			  font-size: 1rem;
			  border: solid 2px #476edb;
			  color: white;
			  padding: 1em 0;
			  float: right;
			  transition: all 0.6s;
			}
			input[type="submit"]:hover, button:hover ,
			input[type="submit"]:active, button:active,
			input[type="submit"]:focus, button:focus {
			  cursor:pointer;
			  outline: none;
			}

			.result{
				text-align: left;
				width: 58%;
			  color: white;
				padding: 30px 40px;
				font-weight: normal;
				line-height: 24px;
				font-size: 15px;
				background: #476edb;
				margin: 50px auto 0;
			}
		</style>
        <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
    </head>
    <body >
      <div class="container-website">
          <div class="left-menu">
              <div class="logo">
                  <img src="assets/images/logo.png" alt="Undefined Hub">
              </div>
              <ul class="links">
                  <li>
                      <a href="/">&larr; Retour au Hub</a>
                  </li>
                   <li>
                      <a href="#">SQL REPLACE</a>
                  </li>
              </ul>
          </div>
          <div class="project-container">
              <?php
			if(!empty($_GET['from']) && !empty($_GET['to'])){
				echo "<div class='result'>";
				$prefix = !empty($_GET['prefix']) ? $_GET['prefix'] : 'wp_';
				echo "UPDATE " . $prefix . "posts SET post_content = REPLACE(post_content, '" . $_GET['from'] . "', '" . $_GET['to'] . "') WHERE post_content LIKE '%" . $_GET['from'] . "%';";
				echo "<br>";
				echo "UPDATE " . $prefix . "posts SET guid = REPLACE(guid, '" . $_GET['from'] . "', '" . $_GET['to'] . "') WHERE guid LIKE '%" . $_GET['from'] . "%';";
				echo "<br>";
				echo "UPDATE " . $prefix . "options SET option_value = REPLACE(option_value, '" . $_GET['from'] . "', '" . $_GET['to'] . "') WHERE option_value LIKE '%" . $_GET['from'] . "%';";
				echo "<br>";
				echo "UPDATE " . $prefix . "postmeta SET meta_value = REPLACE(meta_value, '" . $_GET['from'] . "', '" . $_GET['to'] . "') WHERE meta_value LIKE '%" . $_GET['from'] . "%';";
				echo "<br>";
				echo "UPDATE core_config_data SET value = REPLACE(value, '" . $_GET['from'] . "', '" . $_GET['to'] . "') WHERE value LIKE '%" . $_GET['from'] . "%';";

				echo "</div>";
			}
			?>
			<form>
				<div class="form-group">
					<label>Prefix WP ?</label>
					<input name="prefix" type="text" placeholder="wp_" value="<?php echo !empty($_GET['prefix']) ? $_GET['prefix'] : 'wp_' ?>" />
					<div class="buttons">
						<button data-value="wp_">wp_</button>
					</div>
				</div>
				<div class="form-group">
					<label>From ?</label>
					<input name="from" type="text" placeholder="http://colorz.fr" value="<?php echo !empty($_GET['from']) ? $_GET['from'] : '' ?>" />
					<div class="buttons">
						<button data-value=".dev6.">.dev6.</button>
						<button data-value=".staging6.">.staging6.</button>
						<button data-value=".testing6.">.testing6.</button>
						<button data-value=".feed.">.feed.</button>
						<button data-value=".staging.">.staging.</button>
						<button data-value=".testing.">.testing.</button>
					</div>
				</div>
				<div class="form-group">
					<label>To ?</label>
					<input name="to" type="text" placeholder="http://colorz.dev6.fr" value="<?php echo !empty($_GET['to']) ? $_GET['to'] : '' ?>" />
					<div class="buttons">
						<button data-value=".dev6.">.dev6.</button>
						<button data-value=".staging6.">.staging6.</button>
						<button data-value=".testing6.">.testing6.</button>
						<button data-value=".feed.">.feed.</button>
						<button data-value=".staging.">.staging.</button>
						<button data-value=".testing.">.testing.</button>
					</div>
				</div>
				<input type="submit" value="Valider"/>
			</form>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery('.buttons > button').click(function(){
						jQuery(this).closest('.form-group').find('input[type="text"]').val(jQuery(this).data('value'));
					});
				});
			</script>
          </div>
        </div>
    </body>
</html>
